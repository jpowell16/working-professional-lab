package workingprofessionalapp;

import java.util.ArrayList;
import java.util.Scanner;

import  workingProfessional.*;

;public class Driver 
{
    public static void main( String[] args )
    {
        ArrayList<Tool> tools = new ArrayList<Tool>();
        tools.add(new Wrench("Type 1", 10));
        tools.add(new Wrench("Type 2", 22));
        tools.add(new Wrench("Type 3", 70));
        tools.add(new Pliers("Type 01", "#xxz"));
        tools.add(new Pliers("Type 02", "#123"));
        	
        Scanner scan = new Scanner(System.in);
        PlumbingProfessional pp = null;
        NetworkingProfessional np = null;
        
        String input = " ";
//        String input2 = " ";
        
        while(input.compareToIgnoreCase("Networking")!=0&&input.compareToIgnoreCase("Plumbing")!=0) {
        	if(input.compareToIgnoreCase("Networking")!=0&&input.compareToIgnoreCase("Plumbing")!=0&&input.compareToIgnoreCase(" ")!=0) {
        		System.out.println("Networking and Plumbling are the only valid professions ");
        	}
        	System.out.println("Enter your Profession: ");
        	input = scan.next();
        }
        
        if(input.compareToIgnoreCase("Networking")==0) {
        	System.out.println("Enter your name: ");
        	np = new NetworkingProfessional(scan.nextLine());
        }else {
        	System.out.println("Enter your name: ");
        	pp = new PlumbingProfessional(scan.nextLine());
        }
        
        PvcPipe pipe = null;
        CiscoRouter router = null;
        
        if(input.compareToIgnoreCase("Networking")==0) {
        	router = new CiscoRouter("Cisco", 100, 10);
        	System.out.println("Your Router is being fixed by a Networking Professional");
        	np.fixRouter(router, tools.get(4));
        }else {
        	System.out.println("Enter your name: ");
        	System.out.println("Your Pipe is being fixed by a Plumbing Professional");
        	pp.fixPipe(pipe, tools.get(0));
        }
        
        System.out.println("Amazing Work!");
    }
}
