package workingProfessional;

public class Pliers extends Tool{

	private String colour;
	
	public Pliers(String brand,String colour) {
		super(brand);
		this.colour = colour;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

}

