package workingProfessional;

public interface BreakableItem {

	public void updateDamage(float damage);
	
}
