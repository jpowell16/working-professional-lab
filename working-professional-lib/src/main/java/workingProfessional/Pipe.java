package workingProfessional;

public abstract class Pipe {

	protected float length;
	protected float diameter;
	
	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getDiameter() {
		return diameter;
	}

	public void setDiameter(float diameter) {
		this.diameter = diameter;
	}

	public Pipe(float length, float diameter) {
		super();
		this.length = length;
		this.diameter = diameter;
	}
	
	
	
}

