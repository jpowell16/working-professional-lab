package workingProfessional;

public final class CiscoRouter extends Router{
	
	private int ciscoCode;

	public CiscoRouter(String brand, float bandwidth,int code) {
		super(brand, bandwidth);
		this.ciscoCode = code;
	}

	public int getCiscoCode() {
		return ciscoCode;
	}

	public void setCiscoCode(int ciscoCode) {
		this.ciscoCode = ciscoCode;
	}

}

