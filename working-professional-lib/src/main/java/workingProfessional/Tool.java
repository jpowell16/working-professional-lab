package workingProfessional;

public abstract class Tool {

	private String brand;

	public Tool(String brand) {
		super();
		this.brand = brand;
	}
	
	public boolean fix(BreakableItem item) {
		
		return false;
		
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	
	
}

