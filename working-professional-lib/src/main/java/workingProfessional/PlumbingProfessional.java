package workingProfessional;

public class PlumbingProfessional extends Professional implements IPlumbingProfessional {

	public PlumbingProfessional(String name) {
		super(name);
	}

	
	public boolean fixPipe(Pipe pipe, Tool tool) {
		if(tool instanceof Wrench) {
			System.out.println("The Wrench is being used to fix the Pipe");
			return true;
		}else if(tool instanceof Pliers) {
			System.out.println("The Pliers is being used to fix the Pipe");
			return true;
		}
		return false;
	}
	
}
