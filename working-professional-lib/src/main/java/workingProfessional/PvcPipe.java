package workingProfessional;

public class PvcPipe extends Pipe {

	public String colour;
	
	public PvcPipe(float length, float diameter, String colour) {
		super(length, diameter);
		this.colour = colour;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}
	
}

