package workingProfessional;

public class NetworkingProfessional extends Professional implements INetworkingProfessional {

	public NetworkingProfessional(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public boolean fixRouter(Router router, Tool tool) {
		if(tool instanceof Wrench) {
			System.out.println("The Wrench is being used to fix the Router");
			return true;
		}else if(tool instanceof Pliers) {
			System.out.println("The Pliers is being used to fix the Router");
			return true;
		}
		return false;
	}
	
}
