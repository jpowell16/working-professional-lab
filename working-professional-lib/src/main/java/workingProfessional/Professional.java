package workingProfessional;

import java.util.List;

public abstract class Professional {
	
	private int id;
	private String name;
	private float salary;
	private List<Certification> certification;
	
	public Professional(String name) {
		super();
		this.name = name;
	}

	public void addCertification(Certification certificate) {
		certification.add(certificate);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public List<Certification> getCertification() {
		return certification;
	}

	public void setCertification(List<Certification> certification) {
		this.certification = certification;
	}
	
	

}
